/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package tutorials.java8;

public class AStaticMethodClass {
    
    public static boolean isGreaterThan30(int myNumberToTest) {
        if (myNumberToTest>30)return true;
        else return false;
    }
    
    public static int multiplyBy2(int myNumber) {
        return myNumber*2;
    }

}
