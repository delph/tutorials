/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package tutorials.java8;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.JButton;

public class MainClass {

    public void actionListenerWithAnonymous() {
        JButton button = new JButton();
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("write something");
            }
        });
    }

    public void actionListenerWithLambda1() {
        JButton button = new JButton();
        button.addActionListener(System.out::println);
    }

    // ActionListener interface has only one method, the compiler is not confused.
    public void actionListenerWithLambda() {
        JButton button = new JButton();
        button.addActionListener(e -> {
            System.out.println("write something");
        });

        button.addActionListener(e -> updateData(e));
        button.addActionListener(this::updateData);

    }

    public void listIterationWithoutlambda() {
        List<String> features = Arrays.asList("Lambdas", "Default Method", "Stream API", "Date and Time API");
        for (String feature : features) {
            System.out.println(feature);
        }
    }

    public void listIterationWithlambda() {
        List<String> features = Arrays.asList("Lambdas", "Default Method", "Stream API", "Date and Time API");
        features.forEach(s -> System.out.println(s));
        // or with method reference
        features.forEach(System.out::println);
    }

    private void updateData(ActionEvent e) {
        // do anything
    }

    public static void main(String[] args) {
        

    }

    private static void exampleWithPredicate() {
        List<Integer> myNumbers = Arrays.asList(25, 18, 56, 30, 22, 89);
        System.out.println("Unsing is greater than 30 method");
        printFilteredList(myNumbers, AStaticMethodClass::isGreaterThan30);
    }
    
    private static void exampleWithStreams() {
        List<Integer> myNumbers = Arrays.asList(25, 18, 56, 30, 22, 89);
        List<Integer> newList =  myNumbers.stream().map(AStaticMethodClass::multiplyBy2).collect(Collectors.toList());
        newList.forEach(i->System.out.println("my integer " + i));
    }

    // Predicate is typed, in this case it has to be a function that get Interger as argument and
    // always returns a boolean.
    public static void printFilteredList(List<Integer> myList, Predicate<Integer> condition) {
        for (Integer myInt : myList) {
            if (condition.test(myInt)) {
                System.out.println(myInt);
            }
        }
    }

}
